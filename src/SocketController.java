import model.InvalidIDException;
import model.Player;
import model.RequestType;
import model.Server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.StringTokenizer;

import static java.nio.file.attribute.AclEntryType.DENY;

/**
 * Created by hamon on 4/20/17.
 */
public class SocketController extends Thread{
    Server server;
    Socket socket;
    Player player;
    public SocketController(Server server, Socket socket) {
        this.server = server;
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            Scanner input = new Scanner(socket.getInputStream());
            StringTokenizer in = new StringTokenizer(input.nextLine());
            String id = in.nextToken();
            if(id.equals("NEW")) {
                player = server.newPlayer(in.nextToken());
            }
            else
                player = server.getPlayer(Integer.parseInt(in.nextToken()));
            player.setSocket(socket);
            player.sendMessage("ID " + player.getID());

            while(input.hasNextLine()){
                handleQuery(input.nextLine());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /* Client Queries:
    *       REQUEST <playerID>
    *       ACCEPT <gameID>
    *       DENY <gameID>
    *       PLAYERS
    *       WORD <gameID> <word>
    *       Guess <gameID> <char>
    */
    private void handleQuery(String query) {
        System.out.println(query);
        StringTokenizer st = new StringTokenizer(query);
        try {
            RequestType type = RequestType.valueOf(st.nextToken());
            switch (type){
                case REQUEST:
                    server.gameRequest(player.getID(), Integer.parseInt(st.nextToken()));
                    break;
                case DENY:
                    server.denyRequest(player.getID(), Integer.parseInt(st.nextToken()));
                    break;
                case PLAYERS:
                    server.sendPlayers(player);
                    break;
                case ACCEPT:
                    server.getMatch(Integer.parseInt(st.nextToken())).accept(player);
                    break;
                case WORD:
                    server.getMatch(Integer.parseInt(st.nextToken())).word(player, st.nextToken());
                    break;
                case GUESS:
                    server.getMatch(Integer.parseInt(st.nextToken())).guess(player, st.nextToken().charAt(0));
                    break;
                case FINALGUESS:
                    server.getMatch(Integer.parseInt(st.nextToken())).guessWord(player, st.nextToken());

            }
        }
        catch(IllegalArgumentException e) {
            player.sendMessage("InvalidRequest");
        }
        catch (InvalidIDException e) {
            player.sendMessage( "InvalidID");
        }
    }
}
