import model.Server;

import java.io.IOException;
import java.net.ServerSocket;

public class Main {
    public static void main(String[] args) {
        Server server = new Server();

        try {
            ServerSocket serverSocket = new ServerSocket(1111);
            while (true) {
                new SocketController(server, serverSocket.accept()).start();
                System.out.println("connected!");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
