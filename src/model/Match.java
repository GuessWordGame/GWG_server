package model;

/**
 * Created by hamon on 4/20/17.
 */
public class Match {
    private int ID,gameSets;
    private Player[] players;
    private int[] score;
    private GameStage stage;
    private String gameWord;
    private String guessWord;
    private int guessCounter;

    public Match(Player p1, Player p2, int ID) {
        players = new Player[]{p1,p2};
        score = new int[]{0,0};
        this.ID = ID;
        gameSets = 4;
        stage = GameStage.REQUEST;
    }

    public int getID() {
        return ID;
    }

    public boolean deniableBy(int playerID) {
        return (stage==GameStage.REQUEST && players[1].getID() == playerID);
    }

    public Player getPlayer(int i) {
        return players[i-1];
    }

    public void accept(Player player) {
        //TODO throw Exception: if(stage!=GameStage.REQUEST || p2.equals(player))
        stage = GameStage.CHOOSEWORD1;
        players[0].sendMessage("ACCEPTED "+ID+' '+players[1].getAlias());
        players[1].sendMessage("CHOOSEWORD "+ID);
    }

    public void word(Player player, String word) {
        //TODO handle invalid query
        gameSets--;
        this.gameWord = word;
        this.guessWord = "";
        this.guessCounter = word.length();
        for (int i = 0; i < word.length(); i++)
            guessWord += "?";

        if(stage == GameStage.CHOOSEWORD1)
        {
            stage = GameStage.PLAYER0;
            players[0].sendMessage("GUESS "+this.ID+' '+guessWord);
        }
        if(stage == GameStage.CHOOSEWORD0)
        {
            stage = GameStage.PLAYER1;
            players[1].sendMessage("GUESS "+this.ID+' '+guessWord);
        }
    }

    public void guess(Player player, char c) {
        String tmp = "";
        guessCounter--;
        for (int i = 0; i < gameWord.length(); i++) {
            if(gameWord.charAt(i)==c)
                tmp += c;
            else
                tmp += guessWord.charAt(i);
        }
        guessWord = tmp;
        if(guessCounter>0)
            player.sendMessage("GUESS "+this.ID+' '+guessWord);
        else
            player.sendMessage("FINALGUESS "+this.ID+' '+guessWord);

    }

    public void guessWord(Player player, String s) {
        //TODO check player if he has the move
        if(stage==GameStage.PLAYER0) {
            if(gameWord.compareTo(s)==0) {
                score[0]++;
                players[0].sendMessage("RESULT " + this.ID + " CORRECT");
                players[1].sendMessage("RESULT " + this.ID + " CORRECT");
            }
            else{
                players[0].sendMessage("RESULT " + this.ID + " INCORRECT");
                players[1].sendMessage("RESULT " + this.ID + " INCORRECT");
            }
            if(gameSets>0) {
                stage = GameStage.CHOOSEWORD0;
                players[0].sendMessage("CHOOSEWORD " + ID);
            }
        }
        if(stage==GameStage.PLAYER1) {
            if(gameWord.compareTo(s)==0) {
                score[1]++;
                players[0].sendMessage("RESULT " + this.ID + " CORRECT");
                players[1].sendMessage("RESULT " + this.ID + " CORRECT");
            }
            else{
                players[0].sendMessage("RESULT " + this.ID + " INCORRECT");
                players[1].sendMessage("RESULT " + this.ID + " INCORRECT");
            }
            if(gameSets>0) {
                stage = GameStage.CHOOSEWORD1;
                players[1].sendMessage("CHOOSEWORD " + ID);
            }
        }
        if(gameSets==0)
            this.finish();
    }

    private void finish() {
        players[0].sendMessage("END "+this.ID + ' ' +score[0] + ' ' +score[1] );
        players[1].sendMessage("END "+this.ID + ' ' +score[1] + ' ' +score[0] );
    }
}
