package model;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Queue;
import java.util.concurrent.SynchronousQueue;

/**
 * Created by hamon on 4/20/17.
 */
public class Player {
    int  id;
    Queue<String> messages;
    PrintWriter out;
    String alias;
    private Socket socket;

    public Player(int id, String alias){
        messages = new SynchronousQueue<>();
        this.alias = alias;
        this.id = id;
    }

    public int getID() {
        return id;
    }

    public void sendMessage(String s) {
        //messages.add(s);
        out.write(s+"\n");
        out.flush();
    }

    public String toString() {
        return ""+id+" "+alias;
    }

    public String getAlias() {
        return alias;
    }

    public void setSocket(Socket socket) throws IOException {
        this.socket = socket;
        out = new PrintWriter(socket.getOutputStream());
    }

    public boolean isConnected() {
        return socket.isConnected();
    }
}
