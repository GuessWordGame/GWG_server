package model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Server {
    Map<Integer, Match> matches;
    Map<Integer, Player> players;
    int gameIDCounter, playerIDCounter;
    public Server(){
        matches = new HashMap<>();
        players = new HashMap<>();
        playerIDCounter = gameIDCounter = 1000000;
    }

    public Player newPlayer(String alias) {
        Player tmp = new Player(++playerIDCounter, alias);
        players.put(tmp.getID(),tmp);
        return tmp;
    }

    public Player getPlayer(int id) {
        return players.get(id);
    }

    public void gameRequest(int id1, int id2) throws InvalidIDException {
        if(players.get(id1)==null || players.get(id2)==null)
            throw new InvalidIDException();
        Match match = new Match(players.get(id1),players.get(id2),++gameIDCounter);
        matches.put(gameIDCounter, match);
        players.get(id2).sendMessage("REQUEST "+getPlayer(id1).getAlias()+" "+match.getID());
    }

    public void denyRequest(int playerID, int gameID) {
        //TODO handle invalid requests
        if(matches.get(gameID).deniableBy(playerID)) {
            matches.get(gameID).getPlayer(1).sendMessage("DENIED "+playerID);
            matches.remove(gameID);
        }
    }

    public void sendPlayers(Player player) {
        player.sendMessage("PLAYERS "+(players.size()-1));
        Set keys = players.keySet();
        for(Object id: keys){
            if((int)id != player.getID() && players.get(id).isConnected())
                player.sendMessage(players.get(id).toString());
        }
    }

    public Match getMatch(int id) {
        return matches.get(id);
    }
}
