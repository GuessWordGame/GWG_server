package model;

/**
 * Created by hamon on 4/26/17.
 */
public enum RequestType {
    MOVE,
    GIVEUP,
    FINALMOVE,
    REQUEST,
    ACCEPT,
    DENY, USERS, PLAYERS, WORD, GUESS, FINALGUESS;
}
